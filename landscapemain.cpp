#include "LandscapeWindow.h"

#include <QApplication>
#include <QVBoxLayout>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    //creating the main window
    LandscapeWindow *window = new LandscapeWindow(NULL);

    //resizing the window
    window->resize(1600,900);
    window->show();

    app.exec();

    delete window;

    return 0;
}
