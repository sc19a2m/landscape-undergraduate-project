#ifndef OBJECT_H
#define OBJECT_H


#include <iostream>
#include <fstream>
#include <vector>

#include <glm/ext.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Vertex.h"
#include "OBJLoader.h"
#include "include/GLEW/glew.h"
#include <QGLWidget>

class Object
{
private:

    std::vector<Vertex> vertices;
    std::vector<Vertex> initialVertices;
    unsigned int nrVertices;
    glm::vec3 position;
    GLuint VAO;
    GLuint VBO;
    GLuint textureID;


public:
    Object(const char* filename)
    {
        //setting members to initial values
        this->position = glm::vec3(0.f,0.f,0.f);
        this->VAO = 0;
        this->VBO = 0;
        this->textureID = 0;
        this->nrVertices = 0;

        this->initialVertices = OBJLoad(filename);
        this->vertices = this->initialVertices;

    }

    Object()
    {
        this->position = glm::vec3(0.f,0.f,0.f);
        this->VAO = 0;
        this->VBO = 0;
        this->textureID = 0;
        this->nrVertices = 0;
        this->initialVertices = {};
        this->vertices = {};

    }

    ~Object()
    {

    }

    void reset(){
        this->position = glm::vec3(0.f,0.f,0.f);
        this->VAO = 0;
        this->VBO = 0;
        this->textureID = 0;
        this->nrVertices = 0;
    }

    void loadModel(const char* filename){
        this->initialVertices = OBJLoad(filename);
        this->vertices = this->initialVertices;
    }

    void setModelFromObject(std::vector<Vertex> auxInitialVertices){
        this->initialVertices = auxInitialVertices;
        this->vertices = this->initialVertices;
    }

    void loadRandomModel(int x, int z)
    {
        int value = (x + z) % 7 + 1;
        if(value == 1) {
            this->loadModel("../Landscape/objects/RockSet05A.obj");
        }
        else if (value == 2) {
            this->loadModel("../Landscape/objects/RockSet05B.obj");
        }
        else if (value == 3) {
            this->loadModel("../Landscape/objects/RockSet05C.obj");
        }
        else if (value == 4) {
            this->loadModel("../Landscape/objects/RockSet05D.obj");
        }
        else if (value == 5) {
            this->loadModel("../Landscape/objects/RockSet05E.obj");
        }
        else if (value == 6) {
            this->loadModel("../Landscape/objects/RockSet05F.obj");
        }
        else if (value == 7) {
            this->loadModel("../Landscape/objects/tree.obj");
        }
    }

    void initBuffers()
    {

        glCreateVertexArrays(1, &this->VAO);
        glBindVertexArray(this->VAO);

        glGenBuffers(1, &this->VBO);
        glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
        glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
        glEnableVertexAttribArray(0);
        //Color
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, color));
        glEnableVertexAttribArray(1);
        //Texture Coordinate
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, textureCoordinate));
        glEnableVertexAttribArray(2);
        //Normal
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));
        glEnableVertexAttribArray(3);
    }

    void setTexture(GLuint texID)
    {
        this->textureID = texID;
    }

    void setPosition(glm::vec3 newPosition)
    {
        this->position.x = newPosition.x;
        this->position.y = newPosition.y;
        this->position.z = newPosition.z;

        for(size_t i = 0; i < this->vertices.size(); i++) {

            this->vertices[i].position.x = this->initialVertices[i].position.x + this->position.x;
            this->vertices[i].position.y = this->initialVertices[i].position.y + this->position.y;
            this->vertices[i].position.z = this->initialVertices[i].position.z + this->position.z;
        }
    }

    void showObject()
    {
        glBindVertexArray(this->VAO);
        glDrawArrays(GL_TRIANGLES, 0, this->vertices.size());
    }

    glm::vec3 getPosition(){
        return this->position;
    }

    std::vector<Vertex> getInitialVertices()
    {
        return this->initialVertices;
    }


};

#endif // OBJECT_H
