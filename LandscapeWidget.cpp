#define STB_IMAGE_IMPLEMENTATION

#define MAX_WIDTH 10000
#define MAX_HEIGHT 10000

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

#include <iostream>
#include <fstream>
#include <string.h>
#include "include/GLEW/glew.h"

//#include <vector>

#include "LandscapeWidget.h"

#include "OBJLoader.h"

//#include "Shader.h"
#include "stb_image.h"

#include <QTimer>

#include <time.h>

struct pixelColumn {
    unsigned int width;
    unsigned int height;
    float pointHeight;
};


//constructor
LandscapeWidget::LandscapeWidget(QWidget *parent)
    :QGLWidget(parent)
{
    //Initialize variables

    //Delta Time
    deltaTimer.start();
    this->deltaTime = 0.f;
    this->currentFrameTime = 0.f;
    this->previousFrameTime = 0.f;

    //Map variables
    this->unitWidth = 10;
    this->unitHeight = 10;

    this->setFocusPolicy(Qt::StrongFocus);
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),this,SLOT(update()));
    timer->start();
}

LandscapeWidget::~LandscapeWidget()
{
    delete[] terrainVertices;
    delete[] flatVertices;
    delete[] pixels;
}



void LandscapeWidget::addTexture()
{
    int width , height , bpp ;


    unsigned char* grass_image_data = stbi_load("../Landscape/textures/grass.jpg", &width, &height, &bpp, 0);

    if(!grass_image_data) {
        std::cout << "Can't load texture from grass.jpg \n " << stbi_failure_reason() << std::endl;
        exit(0);
    }

    //generating one texture
    glGenTextures(1, &grassTexture);

    //glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, grassTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0,GL_RGB, GL_UNSIGNED_BYTE, grass_image_data);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    //glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT,16.f);

    glActiveTexture(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    stbi_image_free(grass_image_data);

    unsigned char* rock_image_data = stbi_load("../Landscape/textures/rock.jpg", &width, &height, &bpp, 0);

    if(!rock_image_data) {
        std::cout << "Can't load texture from rock.jpg \n " << stbi_failure_reason() << std::endl;
        exit(0);
    }

    //generating one texture
    glGenTextures(1, &rockTexture);

    //glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, rockTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0,GL_RGB, GL_UNSIGNED_BYTE, rock_image_data);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    //glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT,16.f);

    glActiveTexture(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    stbi_image_free(rock_image_data);

    unsigned char* grayStone_image_data = stbi_load("../Landscape/textures/graystone.jpg", &width, &height, &bpp, 0);

    if(!grayStone_image_data) {
        std::cout << "Can't load texture from gray stone.jpg \n " << stbi_failure_reason() << std::endl;
        exit(0);
    }

    //generating one texture
    glGenTextures(1, &grayStoneTexture);

    //glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, grayStoneTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0,GL_RGB, GL_UNSIGNED_BYTE, grayStone_image_data);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    //glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT,16.f);

    glActiveTexture(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    stbi_image_free(grayStone_image_data);
}


void LandscapeWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_W:
        this->camera->updateKeyboardInput(this->deltaTime,0);
        break;
    case Qt::Key_S:
        this->camera->updateKeyboardInput(this->deltaTime,1);
        break;
    case Qt::Key_A:
        this->camera->updateKeyboardInput(this->deltaTime,2);
        break;
    case Qt::Key_D:
        this->camera->updateKeyboardInput(this->deltaTime,3);
        break;
    case Qt::Key_R:
        this->camera->resetPosition();
        break;

    }
}

void LandscapeWidget::mousePressEvent(QMouseEvent *event)
{
    this->virtualMousePosition = event->pos();
    setCursor(Qt::BlankCursor);
    QCursor::setPos(mapToGlobal(rect().center()));
    //update();
    QWidget::mousePressEvent(event);
}

void LandscapeWidget::mouseReleaseEvent(QMouseEvent *event)
{
    QCursor::setPos(mapToGlobal(this->virtualMousePosition));
    setCursor(Qt::ArrowCursor);
    QWidget::mouseReleaseEvent(event);
}


void LandscapeWidget::mouseMoveEvent(QMouseEvent *event)
{
    if(event->buttons() & Qt::LeftButton && event->pos() != rect().center()) {
        this->virtualMousePosition = (event->pos() - rect().center());
        this->camera->updateInput(this->deltaTime, -1, this->virtualMousePosition.x(), this->virtualMousePosition.y());
        QCursor::setPos(mapToGlobal(rect().center()));
    }
    QWidget::mouseMoveEvent(event);
}


//updates Delta time to separate movements from framerate/sec
void LandscapeWidget::updateDeltaTime()
{

    this->currentFrameTime = this->deltaTimer.elapsed();
    this->deltaTime = this->currentFrameTime - this->previousFrameTime;
    this->previousFrameTime = this->currentFrameTime;

    //resetting value for previousFrameTime and currentFrameTime to stop overflow
    if(this->previousFrameTime > 10000 )
    {
        this->currentFrameTime = this->currentFrameTime - 9000;
        this->previousFrameTime = this->previousFrameTime - 9000;
    }
}

void LandscapeWidget::readHeightMapFromSTBImage(char image_title[20])
{


    char path[100] = "../Landscape/maps/";
    strcat(path,image_title);

    imageWidth = 0;
    imageHeight = 0;

    //loading the image data
    unsigned char* image_data = stbi_load(path, &imageWidth, &imageHeight, &bpp, 1);
    if(!image_data) {
        printf("Error in loading the image \n ");
        exit(1);
    }

    int x,z;
    stbi_uc pixel = NULL;

    //allocating memory for heightMap Array
    pixels = new struct heightMapPixels[imageWidth + 1];
    for(int i = 0; i < imageWidth; i++) {
        pixels[i].column = new struct pixelColumn[imageHeight + 1];
    }

    /*
    for(x = 0; x < imageWidth; x++) {
        for (z = 0; z < imageHeight; z++)
            pixel = image_data[imageWidth * z + x];
    }
    */

    //loading point heights into heightMap struct array
    for(x = 0; x < imageWidth; x++) {
        for(z = 0; z < imageHeight; z++) {
            pixels[x].column[z].width = x;
            pixels[x].column[z].height = z;
            pixels[x].column[z].pointHeight = (float) image_data[imageWidth * z + x];
        }
    }
    //free image_data memory
    stbi_image_free(image_data);

    //Compute Center of Map
    this->centerXpos = (imageWidth * 100)/2;
    this->centerYpos = 0;
    this->centerZpos = (imageHeight * 100)/2;
}

void LandscapeWidget::renderHeightMap(float size, float height)
{
    int counter;

    for(int x = 0; x < imageWidth - 1; x++)
        for(int z = 0; z < imageHeight - 1; z++) {

            glEnable(GL_TEXTURE_2D);

            glBegin(GL_TRIANGLE_STRIP);
            glTexCoord2f( 0.0 , 0.0 );
            glVertex3f(x * size, pixels[x].column[z].pointHeight * height, z * size);
            glTexCoord2f( 1.0 ,0.0 );
            glVertex3f((x + 1) * size, pixels[x + 1].column[z].pointHeight * height, z * size);
            glTexCoord2f( 1.0 , 1.0 );
            glVertex3f(x * size, pixels[x].column[z + 1].pointHeight * height, (z + 1) * size);
            glTexCoord2f( 0.0 , 1.0 );
            glVertex3f((x + 1) * size, pixels[x+1].column[z+1].pointHeight * height, (z + 1) * size);
            glEnd();
        }
}

void LandscapeWidget::setCenterPosition()
{
    this->centerPosition.x = this->unitWidth * this->imageWidth/2;
    this->centerPosition.y = 300 + this->unitHeight * pixels[this->imageWidth/2].column[this->imageHeight/2].pointHeight;
    this->centerPosition.z = this->unitWidth * this->imageHeight/2;
}

glm::vec3 LandscapeWidget::computeSurfaceNormal(glm::vec3 vertex1, glm::vec3 vertex2, glm::vec3 vertex3)
{
    glm::vec3 v1, v2;
    v1.x = vertex2.x - vertex1.x;
    v1.y = vertex2.y - vertex1.y;
    v1.z = vertex2.z - vertex1.z;

    v2.x = vertex3.x - vertex1.x;
    v2.y = vertex3.y - vertex1.y;
    v2.z = vertex3.z - vertex1.z;

    glm::vec3 result = glm::cross(v1,v2);
    result = glm::normalize(result);
    return result;
}

//loads the terrain into the necessary arrays
void LandscapeWidget::loadTerrain(GLfloat size, GLfloat height)
{

    terrainVertices = new Vertex[imageWidth * imageHeight * 7 + 1];
    flatVertices = new Vertex[imageWidth * imageHeight * 7 + 1];
    std::cout <<" Allocated memory for terrain" << std::endl;
    std::cout<< imageWidth << " " <<imageHeight <<std::endl;

    Vertex theVertex1,theVertex2,theVertex3,theVertex4;
    theVertex1.color = glm::vec3(0.f,1.f,0.f);
    theVertex2.color = glm::vec3(0.f,1.f,0.f);
    theVertex3.color = glm::vec3(0.f,1.f,0.f);
    theVertex4.color = glm::vec3(0.f,1.f,0.f);
    nrOfTerrainVertices = 0;
    nrOfFlatVertices = 0;
    this->vertexCount = 0;
    this->uniqueVertexCount = 0;

    for(int x = 0; x < imageWidth - 1; x++)
        for(int z = 0; z < imageHeight - 1; z++) {

            //Corner 1
            theVertex1.position = glm::vec3((GLfloat)(x * size), (GLfloat)(pixels[x].column[z].pointHeight * height), (GLfloat)(z * size));
            theVertex1.textureCoordinate = glm::vec2(0.f, 1.f);
            theVertex1.normal = glm::vec3(0.f,1.f,0.f);

            //Corner 2
            theVertex2.position = glm::vec3((GLfloat)((x + 1) * size), pixels[x + 1].column[z].pointHeight * height,(GLfloat)(z * size));
            theVertex2.textureCoordinate = glm::vec2(0.f, 0.f);
            theVertex2.normal = glm::vec3(0.f,1.f,0.f);

            //Corner 3
            theVertex3.position = glm::vec3((GLfloat)((x + 1) * size), pixels[x + 1].column[z + 1].pointHeight * height,(GLfloat)((z + 1) * size));
            theVertex3.textureCoordinate = glm::vec2(1.f, 0.f);
            theVertex3.normal = glm::vec3(0.f,1.f,0.f);

            // Corner 4
            theVertex4.position = glm::vec3((GLfloat)(x * size), pixels[x].column[z + 1].pointHeight * height,(GLfloat)((z + 1) * size));
            theVertex4.textureCoordinate = glm::vec2(1.f, 1.f);
            theVertex4.normal = glm::vec3(0.f,1.f,0.f);

            //counting number of vertices added
            this->uniqueVertexCount += 4;
            this->vertexCount += 6;

            //TRIANGLE 1 TO ARRAY
            if(theVertex1.position.y - theVertex2.position.y != 0 ||
               theVertex1.position.y - theVertex3.position.y != 0)
            {
                //Compute Normal for Triangle 1
                glm::vec3 firstNormal = this->computeSurfaceNormal(theVertex1.position, theVertex3.position, theVertex2.position);
                theVertex1.normal = firstNormal;
                theVertex2.normal = firstNormal;
                theVertex3.normal = firstNormal;

                terrainVertices[nrOfTerrainVertices] = theVertex1;
                nrOfTerrainVertices++;
                terrainVertices[nrOfTerrainVertices] = theVertex2;
                nrOfTerrainVertices++;
                terrainVertices[nrOfTerrainVertices] = theVertex3;
                nrOfTerrainVertices++;
            }
            else
            {
                flatVertices[nrOfFlatVertices] = theVertex1;
                nrOfFlatVertices++;
                flatVertices[nrOfFlatVertices] = theVertex2;
                nrOfFlatVertices++;
                flatVertices[nrOfFlatVertices] = theVertex3;
                nrOfFlatVertices++;
            }



            //TRIANGLE 2 TO ARRAY
            if(theVertex1.position.y - theVertex3.position.y != 0 ||
               theVertex1.position.y - theVertex4.position.y != 0)
            {
                //Compute Normal for Triangle 2
                glm::vec3 secondNormal = this->computeSurfaceNormal(theVertex1.position, theVertex4.position, theVertex3.position);
                theVertex1.normal = secondNormal;
                theVertex3.normal = secondNormal;
                theVertex4.normal = secondNormal;

                terrainVertices[nrOfTerrainVertices] = theVertex1;
                nrOfTerrainVertices++;
                terrainVertices[nrOfTerrainVertices] = theVertex3;
                nrOfTerrainVertices++;
                terrainVertices[nrOfTerrainVertices] = theVertex4;
                nrOfTerrainVertices++;
            }
            else
            {
                flatVertices[nrOfFlatVertices] = theVertex1;
                nrOfFlatVertices++;
                flatVertices[nrOfFlatVertices] = theVertex3;
                nrOfFlatVertices++;
                flatVertices[nrOfFlatVertices] = theVertex4;
                nrOfFlatVertices++;
            }
        }
}

void LandscapeWidget::printToTxT()
{
    std::ofstream out;
    int i;

    out.open("../Landscape/output/realVertexData.txt");

    if(!out) //file could not be opened
    {
        std::cout<< "Error: realVertexData.txt could not be opened";
        exit(2);
    }
    for(int x = 0; x < imageWidth - 1; x++)
        for(int z = 0; z < imageHeight - 1; z++)
            out << x * this->unitWidth <<" "
                << this->unitHeight * pixels[x].column[z].pointHeight << " "
                << this->unitWidth * z << std::endl;

    int maxHeight = 0;
    for(int x = 0; x < imageWidth - 1; x++)
        for(int z = 0; z < imageHeight - 1; z++)
            if(pixels[x].column[z].pointHeight > maxHeight)
                maxHeight = pixels[x].column[z].pointHeight;

    out.close();
}

void LandscapeWidget::initObjects()
{
    initialObjects[0].reset();
    initialObjects[0].loadModel("../Landscape/objects/RockSet05A.obj");

    initialObjects[1].reset();
    initialObjects[1].loadModel("../Landscape/objects/RockSet05B.obj");

    initialObjects[2].reset();
    initialObjects[2].loadModel("../Landscape/objects/RockSet05C.obj");

    initialObjects[3].reset();
    initialObjects[3].loadModel("../Landscape/objects/RockSet05D.obj");

    initialObjects[4].reset();
    initialObjects[4].loadModel("../Landscape/objects/RockSet05E.obj");

    initialObjects[5].reset();
    initialObjects[5].loadModel("../Landscape/objects/RockSet05F.obj");

    initialObjects[6].reset();
    initialObjects[6].loadModel("../Landscape/objects/RockSet05G.obj");
}

void LandscapeWidget::scatterObjects()
{
    int i;
    int x, z;

    srand((unsigned)time(0));

    for(i = 0 ; i < 1998; i++)
    {
        x = (rand()%imageWidth);
        z = (rand()%imageHeight);
        object[i].reset();
        int value = (x + z) % 7;
        object[i].setModelFromObject(initialObjects[value].getInitialVertices());
        object[i].setPosition(glm::vec3(x * unitWidth,pixels[x].column[z].pointHeight * unitHeight, z * unitWidth));
        object[i].initBuffers();
    }
}

void LandscapeWidget::scatterLoadObjects()
{
    int i;
    int x, z;

    srand((unsigned)time(0));

    for(i = 0 ; i < 1998; i++)
    {
        x = (rand()%imageWidth);
        z = (rand()%imageHeight);
        object[i].reset();
        object[i].loadRandomModel(x, z);
        object[i].setPosition(glm::vec3(x * unitWidth,pixels[x].column[z].pointHeight * unitHeight, z * unitWidth));
        object[i].initBuffers();
    }
}
// called when OpenGL context is set up

void LandscapeWidget::initMatrices()
{
    //Matrices
    position = glm::vec3(0.f);
    rotation = glm::vec3(0.f);
    scale = glm::vec3(1.f);

    //Initialising the Model Matrix and Transformations
    glm::mat4 initMatrix(1.f);
    ModelMatrix = initMatrix;
    ModelMatrix = glm::translate(ModelMatrix, position);
    ModelMatrix = glm::rotate(ModelMatrix, glm::radians(rotation.x), glm::vec3(1.f, 0.f, 0.f));
    ModelMatrix = glm::rotate(ModelMatrix, glm::radians(rotation.y), glm::vec3(0.f, 1.f, 0.f));
    ModelMatrix = glm::rotate(ModelMatrix, glm::radians(rotation.z), glm::vec3(0.f, 0.f, 1.f));
    ModelMatrix = glm::scale(ModelMatrix, scale);

    //Initialising View Matrix
    glm::vec3 worldUp(0.f, 1.f, 0.f);
    glm::vec3 cameraFront(0.f, 0.f, -1.f);

    this->ViewMatrix = initMatrix;
    this->ViewMatrix = this->camera->getViewMatrix();

    //Initialising Perspective Matrix
    fov = 90.f;
    nearPlane = 0.1f;
    farPlane = 10000.f;
    ProjectionMatrix = glm::mat4(1.f);
    ProjectionMatrix = glm::perspective(glm::radians(fov),
                                        static_cast<float>(LandscapeWidget::width())/LandscapeWidget::height(),
                                        nearPlane,
                                        farPlane);
}

void LandscapeWidget::initTerrainBuffers()
{
    //VAO, VBO
    //GEN VAO AND BIND
    glCreateVertexArrays(1, &VAO1);
    glBindVertexArray(VAO1);

    //GEN VBO AND BIND AND SEND DATA
    GLuint VBO1;
    glGenBuffers(1, &VBO1);
    glBindBuffer(GL_ARRAY_BUFFER, VBO1);
    glBufferData(GL_ARRAY_BUFFER, nrOfTerrainVertices * sizeof(Vertex), terrainVertices, GL_STATIC_DRAW);

    //SET VERTEXATTRIBPOINTERS AND ENABLE INPUT ASSEMBLY
    //sending three floats (vertex location coordinates)
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
    glEnableVertexAttribArray(0);
    //Color
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, color));
    glEnableVertexAttribArray(1);
    //Texture Coordinate
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, textureCoordinate));
    glEnableVertexAttribArray(2);
    //Normal
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));
    glEnableVertexAttribArray(3);

    //BIND VAO 0
    glBindVertexArray(0);

    glCreateVertexArrays(1, &VAO2);
    glBindVertexArray(VAO2);

    GLuint VBO2;
    glGenBuffers(1, &VBO2);
    glBindBuffer(GL_ARRAY_BUFFER, VBO2);
    glBufferData(GL_ARRAY_BUFFER, nrOfFlatVertices * sizeof(Vertex), flatVertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
    glEnableVertexAttribArray(0);
    //Color
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, color));
    glEnableVertexAttribArray(1);
    //Texture Coordinate
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, textureCoordinate));
    glEnableVertexAttribArray(2);
    //Normal
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));
    glEnableVertexAttribArray(3);

    glBindVertexArray(0);
}

void LandscapeWidget::initializeGL()
{
    //set the widget background colour
    glClearColor(0.f,0.f,0.f,1.f);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    //Initializing Timer for Loading
    this->loadTimer = new QElapsedTimer();
    this->loadTimer->start();

    //Initializing GLEW
    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        std::cout << "Problem with GLEW " << std::endl << glewGetErrorString(err) << std::endl;
    }
    std::cout << "Status, Using GLEW" << glewGetString(GLEW_VERSION) << std::endl;

    //Initializing Shader
    coreShader = new Shader();

    //Reading the Height Map
    this->readHeightMapFromSTBImage("Transylvania.png");

    this->setCenterPosition();

    this->loadTerrain(this->unitWidth,this->unitHeight);


    //Initializing the camera
    glm::vec3 cameraStartDirection = glm::vec3(1.f,0.f,0.f);
    this->camera = new Camera(this->centerPosition,cameraStartDirection);

    this->initTerrainBuffers();

    //loading objects
    this->initObjects();
    this->scatterObjects();

    //BIND VAO 0
    glBindVertexArray(0);

    this->addTexture();
    this->initMatrices();

    //Lights
    glm::vec3 lightPos0;
    lightPos0 = this->centerPosition;
    lightPos0.y = lightPos0.y + 1000.f;

    //Initialise Uniforms
    glUseProgram(coreShader->program);
    glUniformMatrix4fv(glGetUniformLocation(coreShader->program, "ModelMatrix"), 1, GL_FALSE, glm::value_ptr(this->ModelMatrix));
    glUniformMatrix4fv(glGetUniformLocation(coreShader->program, "ViewMatrix"), 1, GL_FALSE, glm::value_ptr(this->ViewMatrix));
    glUniformMatrix4fv(glGetUniformLocation(coreShader->program, "ProjectionMatrix"), 1, GL_FALSE, glm::value_ptr(this->ProjectionMatrix));

    glUniform3fv(glGetUniformLocation(coreShader->program, "lightPos0"), 1, glm::value_ptr(lightPos0));
    glUniform3fv(glGetUniformLocation(coreShader->program, "cameraPos"), 1, glm::value_ptr(this->camera->getPosition()));
    glUseProgram(0);

    //Printing load time
    std::cout<< "Map size is: " << this->imageWidth <<" x " << this->imageHeight << std::endl;
    std::cout<< "Load TIME is: " << this->loadTimer->elapsed() <<" ms" << std::endl;

    //Initialising Framerate counter
    fpsTimer = new QElapsedTimer();
    fpsTimer->start();
    frameCounter = 0;

//    Printing number of vertices
//     std::cout << "NUMBER OF TOTAL VERTICES " << this->vertexCount
//              << " OF WHICH UNIQUE " << this->uniqueVertexCount
//              << std::endl;

}


// called every time the widget is resized
void LandscapeWidget::resizeGL(int w, int h)
{
    //set the viewport to the entire widget
    glViewport(0,0,w,h);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    //this->lightingSetup();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //glOrtho(-7000.0, 7000.0, -10, 20000.0, -10.0, 20000.0);
    //glOrtho(-7000.0, 7000.0, -10, 20000.0, -10.0, 20000.0);
}

void LandscapeWidget::paintGL()
{

    //OPENGL OPTIONS
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_NORMALIZE);
    glShadeModel(GL_SMOOTH);
    // Setting the matrix mode to model view directly before enabling the depth test
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    this->updateDeltaTime();
    if(this->deltaTime > 10.f)
        this->deltaTime = 10.f;

    //Use a Program
    glUseProgram(coreShader->program);

    //Update uniforms
    glUniform1i(glGetUniformLocation(coreShader->program, "grassTexture"), 0);
    glUniform1i(glGetUniformLocation(coreShader->program, "rockTexture"), 0);
    glUniform1i(glGetUniformLocation(coreShader->program, "grayStoneTexture"), 0);

    //Move, rotate and scale (MODEL MATRIX)
    glm::mat4 initMatrix(1.f);
    ModelMatrix = initMatrix;
    ModelMatrix = glm::translate(ModelMatrix, position);
    ModelMatrix = glm::rotate(ModelMatrix, glm::radians(rotation.x), glm::vec3(1.f, 0.f, 0.f));
    ModelMatrix = glm::rotate(ModelMatrix, glm::radians(rotation.y), glm::vec3(0.f, 1.f, 0.f));
    ModelMatrix = glm::rotate(ModelMatrix, glm::radians(rotation.z), glm::vec3(0.f, 0.f, 1.f));
    ModelMatrix = glm::scale(ModelMatrix, scale);
    glUniformMatrix4fv(glGetUniformLocation(coreShader->program, "ModelMatrix"), 1, GL_FALSE, glm::value_ptr(ModelMatrix));

    //PROJECTION MATRIX
    ProjectionMatrix = glm::mat4(1.f);
    ProjectionMatrix = glm::perspective(glm::radians(fov),
                                        static_cast<float>(LandscapeWidget::width())/LandscapeWidget::height(),
                                        nearPlane,
                                        farPlane);
    glUniformMatrix4fv(glGetUniformLocation(coreShader->program, "ProjectionMatrix"), 1, GL_FALSE, glm::value_ptr(ProjectionMatrix));

    //Camera
    this->ViewMatrix = this->camera->getViewMatrix();
    glUniformMatrix4fv(glGetUniformLocation(coreShader->program, "ViewMatrix"), 1, GL_FALSE, glm::value_ptr(this->ViewMatrix));
    glUniform3fv(glGetUniformLocation(coreShader->program, "cameraPos"), 1, glm::value_ptr(this->camera->getPosition()));



    //Activate Texture
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, rockTexture);

    //Bind vertex array object
    glBindVertexArray(VAO1);
    //Draw
    glDrawArrays(GL_TRIANGLES, 0 , nrOfTerrainVertices);
    //glDrawElements(GL_TRIANGLES, nrOfTerrainIndices, GL_UNSIGNED_INT, 0);
    glBindTexture(GL_TEXTURE_2D, grassTexture);
    glBindVertexArray(VAO2);
    glDrawArrays(GL_TRIANGLES, 0 , nrOfFlatVertices);


    glBindTexture(GL_TEXTURE_2D, grayStoneTexture);

    for(int i = 0; i < 498; i++){
        object[i].setTexture(grayStoneTexture);
        object[i].showObject();
    }

    //computing the framerate
    ++frameCounter;

    if ( fpsTimer->elapsed() >= 1000) {
        double fps = frameCounter / ((double)fpsTimer->elapsed()/1000.0);
        std::cout <<  fps << "+";
        fpsTimer->restart();
        frameCounter = 0;
    }


    glLoadIdentity();

    // flush to screen
    glFlush();

}


