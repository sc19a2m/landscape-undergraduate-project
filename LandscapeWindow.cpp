#include "LandscapeWindow.h"
#include <iostream>

LandscapeWindow::LandscapeWindow(QWidget *parent)
    :QWidget(parent)
{   //constructor

    windowLayout = new QBoxLayout(QBoxLayout:: TopToBottom, this);

    //create the main widget
    landscapeWidget = new LandscapeWidget(this);
    windowLayout ->addWidget(landscapeWidget);

}


LandscapeWindow::~LandscapeWindow()
{   //destructor
    delete landscapeWidget;
    delete windowLayout;
}
