#ifndef VERTEX_H
#define VERTEX_H

#include <glm/ext.hpp>


struct Vertex {
    glm::vec3 position;
    glm::vec3 color;
    glm::vec2 textureCoordinate;
    glm::vec3 normal;
};

#endif // VERTEX_H
