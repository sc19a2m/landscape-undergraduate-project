#ifndef SHADER_H
#define SHADER_H

#include <iostream>
#include <fstream>
#include <string.h>

#include "include/GLEW/glew.h"

class Shader
{
private:
    GLuint vertexShader;
    GLuint fragmentShader;

    bool loadVertexShader()
    {
        bool loadSuccess = true;
        char infoLog[512];
        GLint successState;

        std::string temp = "";
        std::string src = "";

        std::ifstream in_file;

        //Vertex Shader
        in_file.open("../Landscape/vertex_core.glsl");

        if(in_file.is_open())
        {
            while(std::getline(in_file, temp))
            {
                src += temp + "\n";
            }
        }
        else
        {
            std::cout << "ERROR_COULD_NOT_OPEN_VERTEX_SHADER_FILE" << std::endl;
            loadSuccess = false;
        }

        //closing the vertex shader .glsl file
        in_file.close();


        this->vertexShader = glCreateShader(GL_VERTEX_SHADER);
        const GLchar *vertexSource = src.c_str();
        glShaderSource(this->vertexShader, 1, &vertexSource, NULL);
        glCompileShader(this->vertexShader);

        glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &successState);
        if (!successState)
        {
            glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
            std::cout << "ERROR_VERTEX_SHADER_COULD_NOT_BE_COMPILED" << std::endl;
            std::cout << infoLog << std::endl;
            loadSuccess = false;
        }
        return loadSuccess;
    }

    bool loadFragmentShader()
    {
        bool loadSuccess = true;
        char infoLog[512];
        GLint successState;

        std::string temp = "";
        std::string src = "";

        std::ifstream in_file;

        //Fragment
        in_file.open("../Landscape/fragment_core.glsl");

        if(in_file.is_open())
        {
            while(std::getline(in_file, temp))
            {
                src += temp + "\n";
            }
        }
        else
        {
            std::cout << "ERROR_COULD_NOT_OPEN_FRAGMENT_SHADER_FILE" << std::endl;
            loadSuccess = false;
        }
        //closing the fragment shader .glsl file
        in_file.close();

        this->fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar *fragmentSource = src.c_str();
        glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
        glCompileShader(this->fragmentShader);

        glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &successState);
        if (!successState)
        {
            glGetShaderInfoLog(this->fragmentShader, 512, NULL, infoLog);
            std::cout << "ERROR_FRAGMENT_SHADER_COULD_NOT_BE_COMPILED" << std::endl;
            std::cout << infoLog << std::endl;
            loadSuccess = false;
        }

        return loadSuccess;
    }

public:

    GLuint program;

    //constructor
    Shader()
    {

        GLint successState;
        char infoLog[512];

        this->loadVertexShader();
        this->loadFragmentShader();

        //Initialising Program
        this->program = glCreateProgram();
        glAttachShader(this->program, this->vertexShader);
        glAttachShader(this->program, this->fragmentShader);

        glLinkProgram(this->program);

        //ERROR HANDLING
        glGetProgramiv(this->program, GL_LINK_STATUS, &successState);
        if(!successState)
        {
            glGetProgramInfoLog(program, 512, NULL, infoLog);
            std::cout << "ERROR_SHADERS_COULD_NOT_LINK_PROGRAM" << std::endl;
            std::cout << infoLog << std::endl;
        }

        glUseProgram(0);
        //Delete Shaders
        glDeleteShader(this->vertexShader);
        glDeleteShader(this->fragmentShader);
    }

    //destructor
    ~Shader()
    {
        glDeleteProgram(this->program);
    }




};


#endif // SHADER_H
