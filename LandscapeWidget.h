#ifndef LANDSCAPEWIDGET_H
#define LANDSCAPEWIDGET_H


#include <QKeyEvent>
#include <QElapsedTimer>

#include "Shader.h"
#include "Object.h"
#include "Camera.h"
#include "Vertex.h"


#include <windows.h>

#include <QGLWidget>

struct materialStruct;

class LandscapeWidget: public QGLWidget
{
    Q_OBJECT

public:
    LandscapeWidget(QWidget *parent);
    ~LandscapeWidget();

protected:
    //called when OpenGL context is set up
    void initializeGL();
    //called every time the widget is resized
    void resizeGL(int, int);
    //called every time the widget needs painting
    void paintGL();
    void keyPressEvent(QKeyEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

private:

    //VARIABLES
    //Shader
    GLuint core_program;
    Shader* coreShader;

    struct heightMapPixels {
        struct pixelColumn *column;
    } *pixels;

    //Image Properties
    int imageWidth;
    int imageHeight;
    int bpp;

    //FPS Timer
    QElapsedTimer *fpsTimer;
    QElapsedTimer *loadTimer;
    int frameCounter;
    unsigned int vertexCount;
    unsigned int uniqueVertexCount;

    //Map Variables
    GLfloat unitWidth;
    GLfloat unitHeight;
    glm::vec3 centerPosition;

    //triangle buffer
    GLuint VAO1;
    GLuint VAO2;

    //initial object variables
    Object initialObjects[10];

    //objects
    Object object[2000];

    //Matrices
    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 scale;
    glm::mat4 ModelMatrix;
    glm::mat4 ViewMatrix;
    glm::mat4 ProjectionMatrix;
    float fov;
    float nearPlane;
    float farPlane;


    //vertex vectors
    Vertex *terrainVertices;
    unsigned int nrOfTerrainVertices;
    Vertex *flatVertices;
    unsigned int nrOfFlatVertices;


    //Textures
    GLuint grassTexture;
    GLuint rockTexture;
    GLuint grayStoneTexture;

    //Mouse controls
    QPoint virtualMousePosition;

    //Delta Time
    QElapsedTimer deltaTimer;
    float deltaTime;
    float currentFrameTime;
    float previousFrameTime;

    //Camera and properties
    Camera *camera;
    GLfloat centerXpos = 0;
    GLfloat centerYpos = 0;
    GLfloat centerZpos = 0;

    //FUNCTIONS
    void readHeightMapFromSTBImage(char image_title[20]);
    void renderHeightMap(float size, float height);
    glm::vec3 computeSurfaceNormal(glm::vec3 vertex1, glm::vec3 vertex2, glm::vec3 vertex3);
    void loadTerrain(GLfloat size, GLfloat height);
    void initObjects();
    void initMatrices();
    void initTerrainBuffers();
    void updateDeltaTime();
    void addTexture();
    void printToTxT();
    void setCenterPosition();
    void scatterObjects();
    void scatterLoadObjects();

};

#endif // LANDSCAPEWIDGET_H
