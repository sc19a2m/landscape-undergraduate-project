#ifndef OBJLOADER_H
#define OBJLOADER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>

#include "include/GLEW/glew.h"
#include <glm/ext.hpp>

#include "Vertex.h"

static std::vector<Vertex> OBJLoad(const char* filename)
{
    //Vertex variables
    std::vector<GLint> positionIndices;
    std::vector<GLint> texcoordIndices;
    std::vector<GLint> normalIndices;

    std::vector<glm::vec3> vertexPositions;
    std::vector<glm::vec2> vertexTexcoord;
    std::vector<glm::vec3> vertexNormals;

    //Vertex array
    std::vector<Vertex> vertices;

    std::stringstream stream;
    std::ifstream in_file(filename);
    std::string line = "";
    GLint value = 0;

    //reading from OBJ file
    if(!in_file.is_open())
    {
        std::cout << "Could not open .obj file: " << filename << std::endl;
        exit(404);
    }

    while(std::getline(in_file, line))
    {
        std::string keyword = "";
        stream.clear();
        stream.str(line);
        stream >> keyword;

        if(keyword == "v")
        {
            glm::vec3 tempPositions;
            stream >> tempPositions.x >> tempPositions.y >> tempPositions.z;
            vertexPositions.push_back(tempPositions);
        }
        else if(keyword == "vt")
        {
            glm::vec2 tempTexcoord;
            stream >> tempTexcoord.x >> tempTexcoord.y;
            vertexTexcoord.push_back(tempTexcoord);
        }
        else if(keyword == "vn")
        {
            glm::vec3 tempNormals;
            stream >> tempNormals.x >> tempNormals.y >> tempNormals.z;
            vertexNormals.push_back(tempNormals);
        }
        else if(keyword == "f")
        {
            int numberOfValues = 0;
            int counter = 0;
            while(stream >> value)
            {
                if (counter == 0)
                {
                    positionIndices.push_back(value);
                    numberOfValues++;
                }
                else if (counter == 1)
                {
                    texcoordIndices.push_back(value);
                    numberOfValues++;
                }
                else if (counter == 2)
                {
                    normalIndices.push_back(value);
                    numberOfValues++;
                }

                //Characters
                if (stream.peek() == '/')
                {
                    counter++;
                    stream.ignore(1, '/');
                }
                else if (stream.peek() == ' ')
                {
                    counter++;
                    stream.ignore(1,' ');
                }

                if (counter > 2)
                    counter = 0;

                if(numberOfValues > 9) {
                    std::cout <<"OBJLOAD :: Too many vertices in one face. A face can only be a triangle" << std::endl;
                    exit(44);
                }
            }
        }
        else
        {

        }
        //Building final vertices array
        vertices.resize(positionIndices.size(), Vertex());

        for(size_t i = 0; i < vertices.size(); i++)
        {
            vertices[i].position = vertexPositions[positionIndices[i] - 1];
            vertices[i].textureCoordinate = vertexTexcoord[texcoordIndices[i] - 1];
            vertices[i].normal = vertexNormals[normalIndices[i] - 1];
            vertices[i].color = glm::vec3(1.f,1.f,1.f);
        }

    }
    return vertices;
}





#endif // OBJLOADER_H
