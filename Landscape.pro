QT       += widgets opengl gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

windows: { LIBS +=  -lOpenGL32 -lGLU32 }
!windows: { LIBS +=  -lGLU }



# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    LandscapeWidget.cpp \
    LandscapeWindow.cpp \
    landscapemain.cpp

HEADERS += \
    Camera.h \
    LandscapeWidget.h \
    LandscapeWindow.h \
    OBJLoader.h \
    Object.h \
    Shader.h \
    Shader.h \
    Vertex.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

unix|win32: LIBS += -L$$PWD/lib/ -lglew32

INCLUDEPATH += $$PWD/include/GLEW
DEPENDPATH += $$PWD/include/GLEW

DISTFILES += \
    fragment_core.glsl \
    vertex_core.glsl
