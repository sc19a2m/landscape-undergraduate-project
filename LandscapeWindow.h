#ifndef LANDSCAPEWINDOW_H
#define LANDSCAPEWINDOW_H

#include "LandscapeWidget.h"
#include <QBoxLayout>

class LandscapeWindow: public QWidget
{
public:
    //constructor
    LandscapeWindow(QWidget *parent);

    //destructor
    ~LandscapeWindow();

    QBoxLayout *windowLayout;

    LandscapeWidget *landscapeWidget;

    //resets all interface elements
    void ResetInterface();

private:

};

#endif // LANDSCAPEWINDOW_H
